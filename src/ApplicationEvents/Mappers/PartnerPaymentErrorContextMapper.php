<?php
namespace DivorcerPackages\Loggers\ApplicationEvents\Mappers;

use DivorcerPackages\Loggers\ApplicationEvents\EventContextMapper;

class PartnerPaymentErrorContextMapper extends EventContextMapper
{
    protected function getMap(): array
    {
        return [
            'partnerId' => 'val1',
            'orderId'   => 'val2',
            'file'      => 'val3',
            'line'      => 'val4',
            'message'   => 'val5',
        ];
    }
}
