<?php
namespace DivorcerPackages\Loggers\ApplicationEvents\Mappers;

use DivorcerPackages\Loggers\ApplicationEvents\EventContextMapper;

class PartnerPaymentSuccessContextMapper extends EventContextMapper
{
    protected function getMap(): array
    {
        return [
            'order_id'                       => 'val1',
            'partner_id'                     => 'val2',
            'partner_token'                  => 'val3',
        ];
    }
}
