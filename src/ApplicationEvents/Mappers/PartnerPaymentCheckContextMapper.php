<?php
namespace DivorcerPackages\Loggers\ApplicationEvents\Mappers;

use DivorcerPackages\Loggers\ApplicationEvents\EventContextMapper;

class PartnerPaymentCheckContextMapper extends EventContextMapper
{
    protected function getMap(): array
    {
        return [
            'order_id'                       => 'val1',
            'partner_id'                     => 'val2',
            'partner_token'                  => 'val3',
            'session_id'                     => 'val4',
            'session_and_partner_data_check' => 'val5',
            'paid_invoices'                  => 'val6',
            'paid_invoices_check'            => 'val7',
        ];
    }
}
