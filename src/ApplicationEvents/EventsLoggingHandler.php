<?php
namespace DivorcerPackages\Loggers\ApplicationEvents;

use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;
use DivorcerPackages\DAL\Repositories\ApplicationEventsRepository;

class EventsLoggingHandler extends AbstractProcessingHandler{

    public function __construct($level = Logger::DEBUG, $bubble = true) {
        $this->table = 'application_events';
        parent::__construct($level, $bubble);
    }
    
    protected function write(array $record):void
    {
        $mapper = new $record['context']['mapper']();
        $context = array_filter($record['context'], function($key) {
            return $key !== 'mapper';
        }, ARRAY_FILTER_USE_KEY);

        $data = $mapper->mapContextToModel($context);
        $data['event_type'] = $record['message'];
        $data['time_create'] = date('Y-m-d H:i:s');
        
        ApplicationEventsRepository::addEventLog($data);
    }
}
