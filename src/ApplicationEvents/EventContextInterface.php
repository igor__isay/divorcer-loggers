<?php
namespace DivorcerPackages\Loggers\ApplicationEvents;

interface EventContextInterface
{
    public function getEventType(): string;
    public function asArray(): array;
    public function asModel(): array;
}
