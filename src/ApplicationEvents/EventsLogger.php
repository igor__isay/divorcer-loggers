<?php
namespace DivorcerPackages\Loggers\ApplicationEvents;

use Monolog\Logger;

class EventsLogger{
/**
     * Create a custom Monolog instance.
     *
     *
     * @param  array  $config
     * @return \Monolog\Logger
     */
    public function __invoke(array $config){
        $logger = new Logger("EventsLoggingHandler");
        return $logger->pushHandler(new EventsLoggingHandler());
    }
}
