<?php
namespace DivorcerPackages\Loggers\ApplicationEvents;

abstract class EventContextMapper {
    abstract protected function getMap(): array;

    protected function mapper(array $map, array $array): array
    {
        $result = [];
        foreach ($array as $key => $value) {
            $result[$map[$key]] = $value; 
        }
        return $result;
    }

    public function mapModelToContext(array $model): array
    {
        return $this->mapper(array_flip($this->getMap()), $model);
    }
    
    public function mapContextToModel(array $context): array
    {
        return $this->mapper($this->getMap(), $context);
    }
}
