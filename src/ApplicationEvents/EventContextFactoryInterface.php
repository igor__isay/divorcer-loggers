<?php
namespace DivorcerPackages\Loggers\ApplicationEvents;

interface EventContextFactoryInterface
{
    public static function create(): EventContextInterface;
}
