<?php
namespace DivorcerPackages\Loggers\ApplicationEvents\Factories;

use DivorcerPackages\Loggers\ApplicationEvents\EventContext;
use DivorcerPackages\Loggers\ApplicationEvents\EventContextFactoryInterface;
use DivorcerPackages\Loggers\ApplicationEvents\Mappers\PartnerPaymentSuccessContextMapper;

class PartnerPaymentSuccessContextFactory implements EventContextFactoryInterface  {
    public static function create(
        ?int    $orderId                    = null,
        ?int    $partnerId                  = null,
        ?string $partnerToken               = null
    ): EventContext
    {
        return new EventContext(
            'partner_payment_success',
            new PartnerPaymentSuccessContextMapper(),
            [
                'order_id'                       => $orderId,
                'partner_id'                     => $partnerId,
                'partner_token'                  => $partnerToken,
            ],
        );
    }
}
