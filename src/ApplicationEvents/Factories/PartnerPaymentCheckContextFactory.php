<?php
namespace DivorcerPackages\Loggers\ApplicationEvents\Factories;

use DivorcerPackages\Loggers\ApplicationEvents\EventContext;
use DivorcerPackages\Loggers\ApplicationEvents\EventContextFactoryInterface;
use DivorcerPackages\Loggers\ApplicationEvents\Mappers\PartnerPaymentCheckContextMapper;

class PartnerPaymentCheckContextFactory implements EventContextFactoryInterface  {
    public static function create(
        ?int    $orderId                    = null,
        ?int    $partnerId                  = null,
        ?string $partnerToken               = null,
        ?string $sessionId                  = null,
        ?bool   $sessionAndPartnerDataCheck = null,
        ?int    $paidInvoices               = null,
        ?bool   $paidInvoicesCheck          = null
    ): EventContext
    {
        return new EventContext(
            'partner_payment_check',
            new PartnerPaymentCheckContextMapper(),
            [
                'order_id'                       => $orderId,
                'partner_id'                     => $partnerId,
                'partner_token'                  => $partnerToken,
                'session_id'                     => $sessionId,
                'session_and_partner_data_check' => $sessionAndPartnerDataCheck,
                'paid_invoices'                  => $paidInvoices,
                'paid_invoices_check'            => $paidInvoicesCheck,
            ],
        );
    }
}
