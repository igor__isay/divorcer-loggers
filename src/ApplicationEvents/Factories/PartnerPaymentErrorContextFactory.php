<?php
namespace DivorcerPackages\Loggers\ApplicationEvents\Factories;

use DivorcerPackages\Loggers\ApplicationEvents\EventContext;
use DivorcerPackages\Loggers\ApplicationEvents\EventContextFactoryInterface;
use DivorcerPackages\Loggers\ApplicationEvents\Mappers\PartnerPaymentErrorContextMapper;

class PartnerPaymentErrorContextFactory implements EventContextFactoryInterface  {
    public static function create(?string $partnerId = null, ?string $orderId = null, ?string $file = null, ?string $line = null, ?string $message = null): EventContext
    {
        return new EventContext(
            'partner_payment_error',
            new PartnerPaymentErrorContextMapper(),
            ['partnerId' => $partnerId, 'orderId' => $orderId, 'file' => $file, 'line' => $line, 'message' => $message],
        );
    }
}
