<?php
namespace DivorcerPackages\Loggers\ApplicationEvents;

class EventContext implements EventContextInterface {
    private EventContextMapper $mapper;
    private string $eventType;
    private array $context;

    public function __construct(?string $eventType, EventContextMapper $mapper, ?array $context)
    {
        $this->mapper = $mapper;
        $this->eventType = $eventType;
        $this->context = $context;
    }

    public function getEventType(): string {
        return $this->eventType;
    }

    public function asArray(): array
    {
        $array = $this->context;
        $array['mapper'] = get_class($this->mapper);
        return $array;
    }

    public function asModel(): array
    {
        return $this->mapper->mapContextToModel($this->context);
    }
}
